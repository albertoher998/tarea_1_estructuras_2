Tarea #1 de Estructuras de Computadoras 2
Alberto Hernandez Hernandez
B63348

To Compile:
make build

To run the program:
make run

To run the test with the parameters given in the results:
make test

To clean all binaries:
make clean

To build and run:
make