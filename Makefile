
#######################################
# Variables
#######################################

# C compiler
CC=gcc

# Files
BASEDIR=.
INCLDIR=$(BASEDIR)/INCL
SRCDIR=$(BASEDIR)/SRC
BINDIR=$(BASEDIR)/BIN
DOCDIR=$(BASEDIR)/DOC
RESDIR=$(BASEDIR)/RESULT 

OUTFILE=test

# Compiler options and flags
CFLAGS= -g -I $(INCLDIR) -o $(BINDIR)/$(OUTFILE)

all: build run

build:
	@echo "Compiling:"
	$(CC) $(SRCDIR)/*.c  $(CFLAGS) -lm
	@echo "\nReady"
clean:
	@rm -rf $(BINDIR)/*
	@rm -f *~
	@echo "Clean Ready"

run:
	@echo "Executing"
	gunzip -c branch-trace-gcc.trace.gz | ./$(BINDIR)/$(OUTFILE) -s 3 -bp 3 -gh 4 -ph 3 -o 0
	@echo "\nReady" 

test: 
	@echo "Compiling:"
	$(CC) $(SRCDIR)/*.c  $(CFLAGS) -lm
	@echo "\nReady"
	@echo "Ejecutando prueba"
	gunzip -c branch-trace-gcc.trace.gz | ./$(BINDIR)/$(OUTFILE) -s 3 -bp 0 -gh 4 -ph 3 -o 1
	gunzip -c branch-trace-gcc.trace.gz | ./$(BINDIR)/$(OUTFILE) -s 4 -bp 2 -gh 3 -ph 3 -o 1
	gunzip -c branch-trace-gcc.trace.gz | ./$(BINDIR)/$(OUTFILE) -s 5 -bp 1 -gh 3 -ph 2 -o 1
	gunzip -c branch-trace-gcc.trace.gz | ./$(BINDIR)/$(OUTFILE) -s 3 -bp 3 -gh 4 -ph 3 -o 1
	@echo "\nReady"



.PHONY: all clean doc