#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

// defined constants to ease of use

// States
#define SN 0
#define WN 1
#define WT 2
#define ST 3

//arbitrary word size to read
#define WORD_SIZE 14

//amount of addresses to read in caso of a written file needed
#define TEST_SIZE 5000

// create file check
#define CREATE_FILE 1
//terminalPrint is in charge of printing all the information about the execution in terminal
void terminalPrint(char type[], int correctNT, int correctT, int incorrectNT, int incorrectT, int s, int ph, int gh, int o);

//bimodalPredictor is the method the executes the bimodal algorithym to make branch predictions
char bimodalPredictor( int s, int bp, int ph, int gh, int o);

//globalHistoryPredictos is the method the executes the g share algorithym to make branch predictions
char globalHistPredictor(int s, int bp, int ph, int gh, int o);

//privateHistoryPredictos is the method the executes the p share algorithym to make branch predictions
char privateHistPredictor(int s, int bp, int ph, int gh, int o);

//tournamentPredictos is the method the executes the tournament algorithym to make branch predictions
char tournamentPredictor(int s, int bp, int ph, int gh, int o);