#include "../INCL/predictor.h"


void terminalPrint(char predictor_type[], int correct_nt, int correct_t, int incorrect_nt, int incorrect_t, int s, int ph, int gh, int o){
    //gets the total amount of branches
    int BranchNum = correct_nt+correct_t+incorrect_t+incorrect_nt;
    //gets the percentage of correct branches
    double percentage = (double)(correct_t+correct_nt)/(double)BranchNum;
    //prints a table in the terminal with all the information needed
    printf("-----------------------------------------------------------------\n");
    printf("|Prediction Parameters:\t\t\t\t\t\t|\n");
    printf("-----------------------------------------------------------------\n");
    printf("|Branch Prediction Type:\t\t\t%s\t|\n",predictor_type);
    printf("|BHT size (entries):\t\t\t\t\t%d\t|\n", s);
    printf("|Global History Register size:\t\t\t\t%d\t|\n",gh);
    printf("|Private History Register size:\t\t\t\t%d\t|\n",ph);
    printf("-----------------------------------------------------------------\n");
    printf("|Simulation Results:\t\t\t\t\t\t|\n");
    printf("-----------------------------------------------------------------\n");
    // stetic if for the columns to be adjusted
    if(o==CREATE_FILE){
        printf("|Number of branch:\t\t\t\t\t%d\t|\n",BranchNum);
    }else{
       printf("|Number of branch:\t\t\t\t\t%d|\n",BranchNum); 
    }
    printf("|Correct prediction of taken branches:\t\t\t%d\t|\n",correct_t);
    printf("|Incorrect prediction of taken branches:\t\t%d\t|\n",incorrect_t);
    printf("|Correct prediction of not taken branches:\t\t%d\t|\n", correct_nt);
    printf("|Incorrect prediction of not taken branches:\t\t%d\t|\n",incorrect_t);
    printf("|Percentage of correct predictions:\t\t\t%.4f%%|\n",percentage*100);
    printf("-----------------------------------------------------------------\n");
    
}
char bimodalPredictor( int s, int bp, int ph, int gh, int o){
    //index mask is the size of the amount of counters needed for the predictor to work
    //intex mask is also the mask of s bits that will be used to accsess the correct counter
    int index_mask = pow(2,s);
    //counter array is the array that represents all the counters needed fot the predictor
    int counter_array[index_mask];
    //counter_ct is the integer that will count the amount of correct taken branches
    int counter_ct=0;
    //counter_it is the integer that will count the amount of incorrect taken branches
    int counter_it=0;
    //counter_cn is the integer that will count the amount of correct not taken branches
    int counter_cn=0;
    //counter_it is the integer that will count the amount of incorrect not taken branches
    int counter_in=0;
    //total_branches is the integer that will add up every branch taken of the simulation
    int total_branches=0;
    //jump_read is the char that will store the taken value of the branch
    char jump_read;
    //pc_address is a "String" o char * that will store each memory address of all the bhanches as strings
    char pc_address[WORD_SIZE];  
    //predictor_name only stores the name of the predictor algorithm in use
    char predictor_name [] = "    Bimodal    ";
    //fp is the interface use for writing a file, if needed
    FILE *fp; 

    //this for will initialize the counter array in 0 or Strongly not taken for the initial phase of predictions
    for (size_t i = 0; i < index_mask; i++){
        counter_array[i] = SN;           
    }
    //this if checks the o input to see if it's needed to write a file
    if (o==CREATE_FILE){
        //fopen will create the file with the name given for writing the results of the predictor
        fp = fopen("RESULT/Bimodal.txt","w");
        //this is the name of each of the columns in the file
        fprintf (fp, "| PC\t| Outcome\t| Prediction\t| correct/incorrect |\n");
    }
    //this while statement is in charge of reading the inputs given by the terminal until it reaches a EOF or end of file
    while(scanf("%s %c", pc_address, &jump_read)!=EOF){ 
        //pc_address is the long integer where the value of memory addres is stored, it is converted to long int by atol()
        long int pc_address_access = atol(pc_address);
        //masked pc_access is the last s bits from the pc address
        int masked_pc_access = pc_address_access & (index_mask-1);
        //the if statement checks the counter_array to see what prediction will be taken
        //this first statement check for a not taken prediction either SN or WN
        if (counter_array[masked_pc_access]==SN||counter_array[masked_pc_access]==WN){
            // this second if checks the actual branch taken
            // this statement checks for a not taken branch, so the prediction is correct
            if (jump_read == 'N'){
                //then again is checked if its necesary to write a file 
                if (o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "| %li\t| %c\t| N\t| correct\t|\n", pc_address_access, jump_read);
                }
                //the prediction was correct for a not taken branch so counter_cn will add one
                counter_cn++;
                // this if statement check the actual state of the counter in use
                if (counter_array[masked_pc_access]>SN){
                    // the state was WN so the statet of the counter us updated
                    counter_array[masked_pc_access]--;
                }  
            //here the prediction wasn't correct for the branch taken
            }else{
                // every time a wring prediction of a SN or a WN will be add up to the counter_in
                counter_in++;
                // the counter_array state is updated for the next prediction adds one because the predictcion was a wrong not taken
                counter_array[masked_pc_access]++;
                //then again is checked if its necesary to write a file 
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "| %li\t| %c\t| N\t| incorrect\t|\n", pc_address_access, jump_read);
                }
            } 
        // this else if statement check for a WT or a ST prediction 
        }else if (counter_array[masked_pc_access]==WT||counter_array[masked_pc_access]==ST){
            // checks for the actual branch taken
            if (jump_read=='T'){
                // the branch was correct so counter_ct will count the correct branch taken
                counter_ct++;
                //then again is checked if its necesary to write a file 
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "| %li\t| %c\t| T\t| correct\t|\n", pc_address_access, jump_read);
                }          
                // checks if the actual state can be modified      
                if (counter_array[masked_pc_access]<ST){
                    //modifies the actual state
                    counter_array[masked_pc_access]++;
                } 
            // the prediction wasnt correct for the branch taken
            }else{
                // the counter of incorrect taken branches add up one
                counter_it++;
                // the state of the actual counter is modified
                counter_array[masked_pc_access]--;
                // then again is checked if it's necesary to write a file
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "| %li\t| %c\t| T\t| incorrect\t|\n", pc_address_access, jump_read);
                }
            }
        }
        // then again is checked if it's necesary to write a file
        if (o==CREATE_FILE){
            // here is assigned total_branches, adding up all the other branches taken
           total_branches = counter_it+counter_in+counter_cn+counter_ct;
        //    if the amount of taken banches are equal to TEST_SIZE (5000) the scaning ends
           if (total_branches == TEST_SIZE){
               break;
           }
        }
    }
    // here the information needed to be printed in the terminal is sent for printing
    terminalPrint(predictor_name, counter_cn, counter_ct, counter_in, counter_it, s, ph, gh, o);
    // then again is checked if it's necesary to write a file
    if (o==CREATE_FILE){
        // closes the file created so there are no memory leacks
        fclose(fp);
    }
    return ' ';
}

char globalHistPredictor(int s, int bp, int ph, int gh, int o){

    //index mask is the size of the amount of counters needed for the predictor to work
    //intex mask is also the mask of s bits that will be used to accsess the correct counter
    int index_mask = pow(2,s);    
    int gh_mask = pow(2,gh);
    int counter_array[index_mask];
    int counter_ct=0;
    int counter_it=0;
    int counter_cn=0;
    int counter_in=0;
    int total_branches=0;
    //general history register
    int gh_hist = 0;
    char predictor_name [] = "Global History";
    char jump_read;
    char pc_address[WORD_SIZE]; 
    FILE *fp;
    
    for (size_t i = 0; i < index_mask; i++){
        counter_array[i] = SN;//0=Strongly not taken SN            
    }
     // then again is checked if it's necesary to write a file
     if (o==CREATE_FILE){
        fp = fopen("RESULT/GlobalHistory.txt","w");
        fprintf (fp, "| PC\t| Outcome\t| Prediction\t| correct/incorrect |\n");
    } 
    while(scanf("%s %c", pc_address, &jump_read)!=EOF){
        //pc_address is the long integer where the value of memory addres is stored, it is converted to long int by atol()
        long int pc_address_access = atol(pc_address);
        //masked pc_access is the last s bits from the pc address
        int masked_pc_access = pc_address_access & (index_mask-1);
        // masked gh hist is the last gh bits from the history, so olny this bits will be used in the prediction
        int masked_gh_hist = gh_hist & (gh_mask-1);
        //xor result is the xor of the masked gh and pc access, this decides  what counter to use
        int xor_result = masked_gh_hist^masked_pc_access;
        // final address is the masked xor result, so the only bits used are the s bits of this variable
        int final_address = xor_result & (index_mask-1);
        // prediction check
        if (counter_array[final_address]==SN||counter_array[final_address]==WN){
            // correct prediction
            if (jump_read == 'N'){
                // updates the histoy with a 0 that represents a not taken branch
                gh_hist = gh_hist<<1;
                // then again is checked if it's necesary to write a file
                if (o==CREATE_FILE){
                     fprintf (fp, "| %li\t| %c\t| N\t| correct\t|\n", pc_address_access, jump_read);
                }
                //add to the correct counter
                counter_cn++;
                // updates the state
                if (counter_array[final_address]>0){
                    counter_array[final_address]--;
                }                
            }else{
                // updates the history with a 1 that reresents a taken branch
                gh_hist = gh_hist<<1;
                gh_hist++;
                // adds the correct counter
                counter_in++;
                // updates the state
                counter_array[final_address]++;
                // then again is checked if it's necesary to write a file
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "| %li\t| %c\t| N\t| incorrect\t|\n", pc_address_access, jump_read);
                }
            }
        //prediction check
        }else if (counter_array[final_address]==WT||counter_array[final_address]==ST){// 2 = WT, 3 = ST
            // taken branch check
            if (jump_read=='T'){
                // updates history with a 1
                gh_hist = gh_hist<<1;
                gh_hist++;
                //updates the counter
                counter_ct++;
                // then again is checked if it's necesary to write a file
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "| %li\t| %c\t| T\t| correct\t|\n", pc_address_access, jump_read);
                }  
                // checks the actual state and updates it              
                if (counter_array[final_address]<3){
                    counter_array[final_address]++;
                }                
            }else{
                // updates the history with a 0
                gh_hist = gh_hist<<1;
                // updates the counter of the right branch
                counter_it++;
                // update the state
                counter_array[final_address]--;
                // then again is checked if it's necesary to write a file
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "| %li\t| %c\t| T\t| incorrect\t|\n", pc_address_access, jump_read);
                }    
            }   
        }
        // then again is checked if it's necesary to write a file
        if (o==CREATE_FILE){
           total_branches = counter_it+counter_in+counter_cn+counter_ct;
           if (total_branches == TEST_SIZE){
               break;
           }  
        } 
    }
    terminalPrint(predictor_name, counter_cn, counter_ct, counter_in, counter_it, s, ph, gh, o);
    // then again is checked if it's necesary to write a file
    if (o==CREATE_FILE){
        fclose(fp);
    }
    return ' ';
}
char privateHistPredictor(int s, int bp, int ph, int gh, int o){
    //index mask is the size of the amount of counters needed for the predictor to work
    //intex mask is also the mask of s bits that will be used to accsess the correct counter
    int index_mask = pow(2,s);
    int ph_mask = pow(2,ph);
    int counter_array[index_mask];
    // private history table
    int bht[index_mask];
    int counter_ct=0;
    int counter_it=0;
    int counter_cn=0;
    int counter_in=0;
    int total_branches=0;  
    char predictor_name [] = "Private History";
    char jump_read;
    char pc_address[WORD_SIZE]; 
    FILE *fp;
    
    for (size_t i = 0; i < index_mask; i++){
        counter_array[i] = SN;//0=Strongly not taken SN  
        bht[i] = SN;//History starts in 0, Strongly not taken          
    }
    // then again is checked if it's necesary to write a file
    if (o==CREATE_FILE){
        fp = fopen("RESULT/PrivateHistory.txt","w");
        fprintf (fp, "| PC\t| Outcome\t| Prediction\t| correct/incorrect |\n");
    } 
    while (scanf("%s %c", pc_address, &jump_read)!=EOF){
        //pc_address is the long integer where the value of memory addres is stored, it is converted to long int by atol()
        long int pc_address_access = atol(pc_address);
        // bht masked pc access is the last s bits of the pc address
        int bht_pc_access = pc_address_access & (index_mask-1);
        // masked_bht is the last ph bits of the private history in use
        int masked_bht = bht[bht_pc_access] & (ph_mask-1);
        //xor result is the result of the xor of the bht needed bits and the pc access
        int xor_result = masked_bht ^ bht_pc_access;
        // final address is the last s bits needed to access the counter in use
        int final_address = xor_result & (index_mask-1);
        // checks the prediction
        if (counter_array[final_address]==SN||counter_array[final_address]==WN){
            //checks the actual branch
            if (jump_read == 'N'){
                // updates the private history with a 0
                bht[bht_pc_access] = bht[bht_pc_access]<<1;
                // then again is checked if it's necesary to write a file
                if (o==CREATE_FILE){
                     fprintf (fp, "| %li\t| %c\t| N\t| correct\t|\n", pc_address_access, jump_read);
                }
                // updates the counter of the right branch
                counter_cn++;
                // updates the state of the counter
                if (counter_array[final_address]>0){
                    counter_array[final_address]--;
                } 
            // checks the actual branch
            }else{
                // updates the private history in use with a 1
                bht[bht_pc_access] = bht[bht_pc_access]<<1;
                bht[bht_pc_access]++;
                // updates the counter of the right branch
                counter_in++;
                // updates the state of the counter
                counter_array[final_address]++;
                // then again is checked if it's necesary to write a file
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "| %li\t| %c\t| N\t| incorrect\t|\n", pc_address_access, jump_read);
                }
            }  
        // checks the prediction
        }else if (counter_array[final_address]==WT||counter_array[final_address]==ST){
            // checks the actual branch
            if (jump_read=='T'){
                // updates the private history in use with a 1
                bht[bht_pc_access] = bht[bht_pc_access]<<1;
                bht[bht_pc_access]++;
                // updates the counter of the right branch
                counter_ct++;
                // then again is checked if it's necesary to write a file
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "| %li\t| %c\t| T\t| correct\t|\n", pc_address_access, jump_read);
                }   
                // updates the state of the counter in use             
                if (counter_array[final_address]<3){
                    counter_array[final_address]++;
                } 
            // checks the actual branch
            }else{
                // updates the private history in unse with a 0
                bht[bht_pc_access] = bht[bht_pc_access]<<1;
                // updates the counter of the right branch
                counter_it++;
                // updates the state of the counter in use
                counter_array[final_address]--;
                // then again is checked if it's necesary to write a file
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "| %li\t| %c\t| T\t| incorrect\t|\n", pc_address_access, jump_read);
                }
            }
        }
        // then again is checked if it's necesary to write a file
        if (o==CREATE_FILE){
           total_branches = counter_it+counter_in+counter_cn+counter_ct;
           if (total_branches == TEST_SIZE){
               break;
           }
        }
    }
    terminalPrint(predictor_name, counter_cn, counter_ct, counter_in, counter_it, s, ph, gh, o);
    // then again is checked if it's necesary to write a file
    if (o==CREATE_FILE){
        fclose(fp);
    }
    return ' ';
}

char tournamentPredictor(int s, int bp, int ph, int gh, int o){
    //index mask is the size of the amount of counters needed for the predictor to work
    //intex mask is also the mask of s bits that will be used to accsess the correct counter
    int index_mask = pow(2,s);
    int ph_mask = pow(2,ph);
    int bht[index_mask];
    int gh_mask = pow(2,gh);
    int gh_hist = 0;
    int counter_array[index_mask];
    int gcounter_array[index_mask];
    int pcounter_array[index_mask];
    int counter_ct=0;
    int counter_it=0;
    int counter_cn=0;
    int counter_in=0;
    int total_branches=0;
    //prediction taken by each predictors
    char gh_predict = ' ';
    char ph_predict = ' '; 
    //final desition of the tournament predictor
    char final_predict = ' ';
    char predictor_name [] = "   Tournament  ";
    char jump_read;
    char pc_address[WORD_SIZE];
    FILE *fp;

    for (size_t i = 0; i < index_mask; i++){
        counter_array[i] = SN;//0=Strongly not taken SN 
        gcounter_array[i] = SN;//0=Strongly not taken SN 
        pcounter_array[i] = SN;//0=Strongly not taken SN   
        bht[i] = SN;//gh_history starts in 0, Strongly not taken          
    }
    // then again is checked if it's necesary to write a file
    if (o==CREATE_FILE){
        fp = fopen("RESULT/Tournament.txt","w");
        fprintf (fp, "| PC\t| Predictor\t| Outcome\t| Prediction\t| correct/incorrect |\n");
    }
    while (scanf("%s %c", pc_address, &jump_read)!=EOF){
        //pc_address is the long integer where the value of memory addres is stored, it is converted to long int by atol()
        long int pc_address_access = atol(pc_address);
        // bht masked pc access is the last s bits of the pc address
        int masked_pc_access = pc_address_access&(index_mask-1);
        // gh selection logic (same code as in gh)
        int masked_gh_hist = gh_hist&(gh_mask-1);
        int gh_xor_result = masked_gh_hist ^masked_pc_access;
        int gh_final_address = gh_xor_result &  ( index_mask-1);
        //ph selection logic (same code as in ph)
        int masked_bht = bht[masked_pc_access]&( ph_mask-1);        
        int ph_xor_result = masked_bht ^ masked_pc_access;
        int ph_final_address = ph_xor_result & (index_mask-1);

        //Gshare prediction and state update logic (same as gh code)
        if (gcounter_array[gh_final_address]==SN||gcounter_array[gh_final_address]==WN){// 0 = SN, 1 = WN
            gh_predict = 'N';
            if (jump_read == 'N'){
                gh_hist = gh_hist<<1;
                if (gcounter_array[gh_final_address]>0){
                    gcounter_array[gh_final_address]--;
                }                
            }else{  
                gh_hist = gh_hist<<1;
                gh_hist++;              
                gcounter_array[gh_final_address]++;                
            }            
        }else if (gcounter_array[gh_final_address]==WT||gcounter_array[gh_final_address]==ST){// 2 = WT, 3 = ST
            gh_predict = 'T';
            if (jump_read=='T'){  
                gh_hist = gh_hist<<1;
                gh_hist++;              
                if (gcounter_array[gh_final_address]<3){
                    gcounter_array[gh_final_address]++;
                }                
            }else{ 
                gh_hist = gh_hist<<1;               
                gcounter_array[gh_final_address]--;                
            }
        }

        //Pshare prediction and state update logic (same as ph code)
        if (pcounter_array[ph_final_address]==SN||pcounter_array[ph_final_address]==WN){// 0 = SN, 1 = WN
            ph_predict = 'N';
            if (jump_read == 'N'){
                bht[masked_pc_access] = bht[masked_pc_access]<<1;
                if (pcounter_array[ph_final_address]>0){
                    pcounter_array[ph_final_address]--;
                }                
            }else{  
                bht[masked_pc_access] = bht[masked_pc_access]<<1;
                bht[masked_pc_access]++;              
                pcounter_array[ph_final_address]++;                
            }            
        }else if (pcounter_array[ph_final_address]==WT||pcounter_array[ph_final_address]==ST){// 2 = WT, 3 = ST
            ph_predict = 'T';
            if (jump_read=='T'){  
                bht[masked_pc_access] = bht[masked_pc_access]<<1;
                bht[masked_pc_access]++;             
                if (pcounter_array[ph_final_address]<3){
                    pcounter_array[ph_final_address]++;
                }                
            }else{ 
                bht[masked_pc_access] = bht[masked_pc_access]<<1;             
                pcounter_array[ph_final_address]--;                
            }
        }

        //Tournament

        //checks wich type of prediction will be made
        if (counter_array[masked_pc_access]==SN||counter_array[masked_pc_access]==WN){
            // ph is used to predict not taken branches
            final_predict = ph_predict; 
            // then again is checked if it's necesary to write a file
            if (o==CREATE_FILE){
                fprintf (fp, "| %li\t| P\t| %c\t|", pc_address_access, jump_read);
            }
        //checks wich type of prediction will be made
        }else if (counter_array[masked_pc_access]==WT||counter_array[masked_pc_access]==ST){
            // gh is used to to predict taken branches
            final_predict = gh_predict;
            // then again is checked if it's necesary to write a file
            if(o==CREATE_FILE){
                // the file is written withe the correct information
                fprintf (fp, "| %li\t| G\t| %c\t|", pc_address_access, jump_read);
            }
        }
        //check the final prediction of the tournament (both ph and gh)
        if (final_predict == 'N'){
            //checks the actual branch
            if (jump_read == 'N'){
                
                // then again is checked if it's necesary to write a file
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "N\t| correct\t|\n");
                }
                // updates the counter of the branch
                counter_cn++;
            }else{
                // updates the counter of the branch
                counter_in++;
                // then again is checked if it's necesary to write a file
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "N\t| incorrect\t|\n");
                }
            }         
        }else{
            // Checks the actual branch
            if (jump_read=='T'){
                // update the counter of the branch
                counter_ct++;
                // then again is checked if it's necesary to write a file
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "T\t| correct\t|\n");
                }
            }else{
                // updates the counter of the branch
                counter_it++;
                // then again is checked if it's necesary to write a file
                if(o==CREATE_FILE){
                    // the file is written withe the correct information
                    fprintf (fp, "T\t| incorrect\t|\n");
                }
            } 
        }
        //updates the counter in use in case gshare and pshare have diferent predictions
        if (gh_predict!=ph_predict){
            if (gh_predict==jump_read){
                if (counter_array[masked_pc_access]<3){
                    counter_array[masked_pc_access]++;
                }
                
            }else{
                if (counter_array[masked_pc_access]>0){
                    counter_array[masked_pc_access]--;
                }
                
            }
            
        }
        // then again is checked if it's necesary to write a file
        if (o==CREATE_FILE){
            total_branches = counter_it+counter_in+counter_cn+counter_ct;
            if (total_branches == TEST_SIZE){
                break;
            }
        }
        
    }
    
    
    terminalPrint(predictor_name, counter_cn, counter_ct, counter_in, counter_it, s, ph, gh, o);
    // then again is checked if it's necesary to write a file
    if (o==CREATE_FILE){
        fclose(fp);
    } 
    return ' ';
}

int main(int argc, char *argv[]){
    //variables where parameters will be stored
    int s, bp, ph, gh, o;   
    for (size_t i = 1; i < argc; i++){
        // check if the parameters exist
        if (strcmp(argv[i], "-s")==0){
            s=atoi(argv[i+1]);
        }else if (strcmp(argv[i], "-bp")==0){
            bp=atoi(argv[i+1]);
        }else if (strcmp(argv[i], "-ph")==0){
            ph=atoi(argv[i+1]);
        }else if (strcmp(argv[i], "-gh")==0){
            gh=atoi(argv[i+1]);
        }else if (strcmp(argv[i], "-o")==0){
            o=atoi(argv[i+1]);
        }else{
            // nothing happen
        }        
    }
    
    //switch to access each function depending on bp parameter
    switch (bp){
    case 0:        
        bimodalPredictor(s, bp, ph, gh, o);
        break;
    
    case 1:
       globalHistPredictor(s, bp, ph, gh, o);
        break;
    
    case 2:
       privateHistPredictor(s, bp, ph, gh, o);
        break;
    
    case 3:
       tournamentPredictor(s, bp, ph, gh, o);
        break;

    default:
        // nothing happen
        break;
    }
    return 0;  
}
